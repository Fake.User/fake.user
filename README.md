# HELLO FRIEND, CLICK "READ MORE" TO SEE SOME PROJECTS

[<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/destruct%20-%20frontend.webp" alt="destruct - frontend" width="100%">](https://destruct.dev/)

# DESTRUCT
- DESTRUCT is an audio library for music people
- The samples are recorded / synthesized from scratch
- You can find the live website at [destruct.dev](https://destruct.dev)

The project has a few repositories which can all be found in the [Destruct](https://gitlab.com/destruct) group:

<table>
    <tr>
        <td>[Destruct Frontend](https://gitlab.com/destruct/destruct-web)</td>
        <td>[Destruct Upload](https://gitlab.com/destruct/destruct-upload)</td>
        <td>[Destruct Desktop](https://gitlab.com/destruct/destruct-desktop)</td>
        <td>[Destruct Randomize](https://gitlab.com/destruct/destruct-randomize)</td>
        <td>[Destruct Icons](https://gitlab.com/destruct/destruct-icons)</td>
    </tr>
</table>

[<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/fakeart%20-%20frontend.webp" alt="fakeart" width="100%">](https://fakeart.online)

# FAKEART
FAKE ART is my outdated art porfolio.

You can find the live website at [fakeart]()

You can find the repository here:
<table><tr><td>[FAKEART](https://gitlab.com/Fake.User/fake-art)</td></tr></table>

[<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/deadbeat%20-%20resume.webp" alt="deadbeat resume" width="100%">]()

# DEADBEAT
DEADBEAT is a terrible resume with a pretty fun interactive canvas.

You can find the live website at [deadbeat]()

You can find the repository here:
<table><tr><td>[DEADBEAT](https://gitlab.com/Fake.User/deadbeat)</td></tr></table>
